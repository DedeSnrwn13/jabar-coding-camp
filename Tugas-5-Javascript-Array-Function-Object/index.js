// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();

daftarHewan.forEach(function (hewan) {
  console.log(hewan);
});

// // Soal 2
function introduce(data) {
  return (
    "Nama saya " +
    data.name +
    ", umur saya " +
    data.age +
    " tahun, alamat saya di " +
    data.address +
    ", dan saya punya hobby yaitu " +
    data.hobby
  );
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3
function hitung_huruf_vokal(kata) {
  var hitung = 0;
  var hurufVokal = ["a", "i", "u", "e", "o", "A", "I", "U", "E", "O"];

  for (var i = 0; i < kata.length; i++) {
    if (hurufVokal.indexOf(kata[i]) > -1) {
      hitung++;
    }
  }
  return hitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1, hitung_2); // 3 2

// Soal 4
function hitung(angka) {
  return angka * 2 - 2;
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
