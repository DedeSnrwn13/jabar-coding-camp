// Soal 1
function next_date(tanggal, bulan, tahun) {
  if (bulan == 1) {
    if (tanggal < 31) {
      tanggal++;
      bulan = "Januari";
    } else if (tanggal >= 31) {
      tanggal = 1;
      bulan = "Februari";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 2) {
    if (tanggal < 28) {
      tanggal++;
      bulan = "Februari";
    } else if (tanggal >= 28) {
      tanggal = 1;
      bulan = "Maret";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 3) {
    if (tanggal < 31) {
      tanggal++;
      bulan = "Maret";
    } else if (tanggal >= 31) {
      tanggal = 1;
      bulan = "April";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (tanggal == 4) {
    if (tanggal < 30) {
      tanggal++;
      bulan = "April";
    } else if (tanggal >= 30) {
      tanggal = 1;
      bulan = "Mei";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 5) {
    if (tanggal < 31) {
      tanggal++;
      bulan = "Mei";
    } else if (tanggal >= 31) {
      tanggal = 1;
      bulan = "Juni";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 6) {
    if (tanggal < 30) {
      tanggal++;
      bulan = "Juni";
    } else if (tanggal >= 30) {
      tanggal = 1;
      bulan = "Juli";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 7) {
    if (tanggal < 31) {
      tanggal++;
      bulan = "Juli";
    } else if (tanggal >= 31) {
      tanggal = 1;
      bulan = "Oktober";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 8) {
    if (tanggal < 31) {
      tanggal++;
      bulan = "Oktober";
    } else if (tanggal >= 31) {
      tanggal = 1;
      bulan = "September";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 9) {
    if (tanggal < 30) {
      tanggal++;
      bulan = "September";
    } else if (tanggal >= 30) {
      tanggal = 1;
      bulan = "Oktober";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 10) {
    if (tanggal < 31) {
      tanggal++;
      bulan = "Oktober";
    } else if (tanggal >= 31) {
      tanggal = 1;
      bulan = "November";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 11) {
    if (tanggal < 30) {
      tanggal++;
      bulan = "November";
    } else if (tanggal >= 30) {
      tanggal = 1;
      bulan = "Desember";
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  } else if (bulan == 12) {
    if (tanggal < 31) {
      tanggal++;
      bulan = "Desember";
    } else if (tanggal >= 31) {
      tanggal = 1;
      bulan = "Januari";
      tahun++;
    } else {
      console.log("Oops! Terjadi kesalahan");
    }
  }

  return tanggal + " " + bulan + " " + tahun;
}
var tanggal = 31;
var bulan = 12;
var tahun = 2020;

console.log(next_date(tanggal, bulan, tahun));

// Soal 2
function jumlah_kata(kalimat) {
  return console.log(kalimat.split(" ").length);
}

var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok";
var kalimat_2 = "Saya Iqbal";
var kalimat_3 = "Saya Muhammad Iqbal Mubarok";

jumlah_kata(kalimat_1);
jumlah_kata(kalimat_2);
jumlah_kata(kalimat_3);
