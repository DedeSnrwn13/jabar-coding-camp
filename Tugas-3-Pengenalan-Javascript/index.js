// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var ketiga = pertama.substring(0, 4);
var keempat = pertama.substring(11, 19);
var kelima = kedua.substring(0, 8);
var keenam = kedua.substring(8, 18);
var ketujuh = ketiga.concat(keempat);
var kedelapan = kelima.concat(keenam.toUpperCase());

console.log(ketujuh.concat(kedelapan));


// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var Number1 = parseInt(kataPertama);
var Number2 = parseInt(kataKedua);
var Number3 = parseInt(kataKetiga);
var Number4 = parseInt(kataKeempat);

console.log((Number1 - Number4) * (Number3 + Number2));


// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);