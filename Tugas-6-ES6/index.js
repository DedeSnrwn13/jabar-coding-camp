// Soal 1
const luasPersegiPanjang = () => {
  let panjang = 10;
  let lebar = 3;

  return panjang * lebar;
};

const kelilingPersegiPanjang = () => {
  let panjang = 10;
  let lebar = 3;

  return 2 * (panjang + lebar);
};

console.log(luasPersegiPanjang());
console.log(kelilingPersegiPanjang());

// Soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

//Driver Code
newFunction("William", "Imoh").fullName();

// Soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;

// Driver code
console.log(firstName, lastName, address, hobby);

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

//Driver Code
console.log(combined);

// Soal 5
const planet = "earth";
const view = "glass";

const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(before);
