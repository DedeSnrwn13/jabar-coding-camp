// Soal 1
var nilai;
nilai = 90;

if (nilai >= 85) {
  console.log("indeksnya A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("indeksnya B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("indeksnya C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("indeksnya D");
} else {
  console.log("indeksnya E");
}

// Soal 2
var tanggal = 27;
var bulan = 6;
var tahun = 2002;

switch (bulan) {
  case 1:
    console.log(tanggal + " Januari " + tahun);
    break;
  case 2:
    console.log(tanggal + " Februari " + tahun);
    break;
  case 3:
    console.log(tanggal + " Maret " + tahun);
    break;
  case 4:
    console.log(tanggal + " April " + tahun);
    break;
  case 5:
    console.log(tanggal + " Mei " + tahun);
    break;
  case 6:
    console.log(tanggal + " Juni " + tahun);
    break;
  case 7:
    console.log(tanggal + " Juli " + tahun);
    break;
  case 8:
    console.log(tanggal + " Agustus " + tahun);
    break;
  case 9:
    console.log(tanggal + " September " + tahun);
    break;
  case 10:
    console.log(tanggal + " Oktober " + tahun);
    break;
  case 11:
    console.log(tanggal + " Nopember " + tahun);
    break;
  case 12:
    console.log(tanggal + " Desenber " + tahun);
    break;

  default:
    console.log(tanggal + " " + bulan + " " + tahun);
    break;
}

// Soal 3
for (var n = "#"; n.length <= 3; n += "#") {
  console.log(n);
}

console.log("------");

for (var n = "#"; n.length <= 7; n += "#") {
  console.log(n);
}

// Soal 4
var m = 7;
var kalimat1 = "I love programming";
var kalimat2 = "I love Javascript";
var kalimat3 = "I love VueJS";
var pemisah = "";

for (nomer = 1; nomer <= m; nomer++) {
  if (nomer % 3 == 1) {
    console.log(nomer + " - " + kalimat1);
    pemisah += "=";
  } else if (nomer % 3 == 2) {
    console.log(nomer + " - " + kalimat2);
    pemisah += "=";
  } else if (nomer % 3 == 0) {
    console.log(nomer + " - " + kalimat3);
    pemisah += "=";
    console.log(pemisah);
  }
}
